#include <stdio.h>
#define bool int
#define true 1
#define false 0


bool isInRange(int lowerLimit, int upperLimit, int no)
{
    return (lowerLimit < no && no < upperLimit);
}

int main()
{
    
    int l, u, n;

   
    printf("Enter the lower limit : \n");
    scanf("%d", &l);

    printf("Enter the upper limit : \n");
    scanf("%d", &u);

    printf("Enter the number : \n");
    scanf("%d", &n);

   
    if (isInRange(l, u, n) == 1)
    {
        printf("The number is in the range.\n");
    }
    else
    {
        printf("The number is not in the range.\n");
    }

    return 0;
}
OUTPUT
Enter the lower limit :
1
Enter the upper limit :
10
Enter the number :
4
The number is in the range.

Enter the lower limit :
1
Enter the upper limit :
10
Enter the number :
12
The number is not in the range
