#include<stdio.h>
#include<conio.h>

void swap(int *num1, int *num2);

void main() {
   int x, y;

   printf("\nEnter First number : ");
   scanf("%d", &x);

   printf("\nEnter Second number : ");
   scanf("%d", &y);

   printf("\nBefore Swaping x = %d and y = %d", x, y);
   swap(&x, &y);

   printf("\nAfter Swaping x = %d and y = %d", x, y);
   getch();
}

void swap(int *num1, int *num2) {
   int temp;
   temp = *num1;
   *num1 = *num2;
   *num2 = temp;
}
Output :

Enter First number : 12
Enter Second number : 21

Before Swaping x = 12 and y = 21
After Swaping x = 21 and y = 12