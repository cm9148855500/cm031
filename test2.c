  #include <stdio.h>
  int main() 
  {
        int hour, minute, second, timeinsec;

        printf("Enter the value for hour:");
        /* get hour value from user*/
        scanf("%d", &hour);

        printf("Enter the value for minute:");
        /* get minute value from user */
        scanf("%d", &minute);

        printf("Enter the value for seconds:");
        /* get sec value from the user */
        scanf("%d", &second);

        /* calculate total seconds */
        timeinsec = second + (minute * 60) + (hour * 60 * 60);
        printf("Total seconds in %02dH:%02dM:%02dS is %d\n",
                hour, minute, second, timeinsec);
        return 0;
  }



  Output
  Enter the value for hour:2
  Enter the value for minute:30
  Enter the value for seconds:4
  Total seconds in 02H:30M:04S is 9004