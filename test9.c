
#include <stdio.h>

void swap(int *x,int *y)
{
    int t;
     t   = *x;
    *x   = *y;
    *y   =  t;
}

int main()
{
    int num1,num2;

    printf("Enter value of num1: ");
    scanf("%d",&num1);
    printf("Enter value of num2: ");
    scanf("%d",&num2);
    printf("Before Swapping: num1 is: %d, num2 is: %d\n",num1,num2);

    swap(&num1,&num2);

    
    printf("After  Swapping: num1 is: %d, num2 is: %d\n",num1,num2);

    return 0;
}
Output:
C Program to swap two numbers using point
Enter value of num1:100
Enter value of num2:200
Before swapping:num1 is :100,num2 is :200
After swapping:num1 is :200,num2 is :100